from fuzzywuzzy import fuzz
from fuzzywuzzy import process

import json

def pickModule(jsonFilename):
	with open(jsonFilename) as f:
		options = json.loads(f.read())

	data = []

	for option in options:
		module = option.get("module")
		func = option.get("function")
		keywords = option.get("keywords")

		data.append({"module":module, "func":func, "keywords":keywords})

	return data

def pickBestMatch(modules, question):

	sentences = []

	for module in modules:
		for sentence in module["keywords"]:
			sentences.append(sentence)

	best_match = process.extractOne(question, sentences)
	print(best_match[0])

	for module in modules:
		for sentence in module["keywords"]:
			if best_match[0] == sentence:
				if best_match[1] > 60:
					return module
				else:
					return {
						"func": "overview_tommys",
						"keywords": ["what do you know about Tommy's"],
						"module": "skills.utils"
					}
