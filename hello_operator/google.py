import subprocess
from pygame import mixer

def assistant():
     subprocess.Popen("./send_to_google.sh", shell=True, stdout=subprocess.PIPE)
     mixer.music.load('tmp/google_output.wav')
     mixer.music.play()
     while mixer.music.get_busy() == True:
         continue
     return ""
