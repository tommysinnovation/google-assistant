import socket
import os
import subprocess
dir = os.path.dirname(__file__)

def ip_addr():
    '''
    What is your ip address?
    '''
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    ip_address = s.getsockname()[0]
    answer = "My IP address is " + ip_address
    return answer

def overview_tommys():
    '''
    What do you know about Tommy's?
    '''
    with open(dir + '/speeches/tommys_overview.txt', 'r') as speech:
        return speech.read()

def wifi_password():
    '''
    what is the wifi password?
    '''
    return "you must ask reception for the wifi password"

def keyboard_mice():
    '''
    where are the keyboards/mice?
    '''
    return "Keyboards and mice can be found in the large storage unit under the TV in the lounge area."


def toilets():
    '''
    Where are the toilets?
    '''
    return "toilets can be found opporsite the elevator!"

def temp():
    '''
    gets board temp
    '''
    temp =  subprocess.Popen("/opt/vc/bin/vcgencmd measure_temp", shell=True, stdout=subprocess.PIPE).stdout.read()
    return temp

def bike_parking():
    '''
    Where can I leave my bike?
    '''
    return "There is a garage at the rear of the building - use the intercom to talk to security, tell them you are here for QA training and are on a bike. The bike storage is round to the right"

def where_can_i_park():
    '''
    Where can I park?
    '''
    return "There is a pay and display over the road"

def opening_times():
    '''
    What are the opening times?
    '''
    return "0 800 to 17 30"

def can_i_take_kit_home():
    '''
    Can I take kit home with me?
    '''
    return "We can't let out macbooks - anything else can be booked out via the front desk"

def use_personal_accounts():
    '''
    Can I use my personal accounts?
    '''
    return "yes, if you are happy to be associated with the main building"
