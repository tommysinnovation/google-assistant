#!/usr/bin/env python

import datetime
import json
import os
dir = os.path.dirname(__file__)


def find_next_bus_time():
    '''
    find the next bus time and return it as a string to be spoken
    if there is an error that will be returned as a string
    '''
    try:
        with open(dir+'/timetable.json', 'r') as f:
            bustimes = json.load(f)
        timedifferences = {}
        currentdatetime = datetime.datetime.now()
        currentdate = datetime.date.today()
        for time in bustimes:
            timeobj = datetime.datetime.strptime(time, '%H:%M').time()
            datetimeobj = datetime.datetime.combine(currentdate, timeobj)
            currentdelta = datetimeobj - currentdatetime
            if currentdelta.total_seconds() > 0:
                timedifferences[currentdelta] = time
        closestdelta = min(timedifferences.keys())
        closestbustime = timedifferences[closestdelta].replace(':', ' ')
        etamin = closestdelta.seconds / 60
        say = 'the next bus is at {} you have {} minutes'.format(closestbustime, etamin)
        return say
    except Exception as error:
        say = 'Oh no something went wrong the exception raised is' + str(error)
        return say
