import feedparser


def topnews():
    feed = feedparser.parse('http://feeds.bbci.co.uk/news/rss.xml')
    return feed['entries'][0]['summary']

