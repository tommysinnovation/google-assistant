from gtts import gTTS
from pygame import mixer
import speech_recognition as sr

mixer.init()

def speak(text, slow=False):
    tts = gTTS(text=text, lang='en', slow=slow)
    tts.save("tmp/speech.mp3")

    mixer.music.load('tmp/speech.mp3')
    mixer.music.play()
    while mixer.music.get_busy() == True:
        continue

def speech2text():
    # obtain audio from the microphone
    r = sr.Recognizer()
    with sr.Microphone() as source:
        print("Say something!")

        # recognize speech using Google Speech Recognition
        try:
            audio = r.listen(source, timeout=2)
            with open("tmp/speech_audio.wav", "wb") as f:
                f.write(audio.get_wav_data())
            # for testing purposes, we're just using the default API key
            # to use another API key, use `r.recognize_google(audio, key="GOOGLE_SPEECH_RECOGNITION_API_KEY")`
            # instead of `r.recognize_google(audio)`
            text = r.recognize_google(audio,language="en-GB")
            print("Google Speech Recognition thinks you said " + text)
            return text
        except sr.WaitTimeoutError:
            print("Timeout")
            return None
        except sr.UnknownValueError:
            print("Google Speech Recognition could not understand audio")
            return None
        except sr.RequestError as e:
            print("Could not request results from Google Speech Recognition service; {0}".format(e))

            # recognize speech using Microsoft Bing Voice Recognition
            BING_KEY = "e4b8ed692d6045369bd8e656c725f9c3"  # Microsoft Bing Voice Recognition API keys 32-character lowercase hexadecimal strings
            try:
                text = r.recognize_bing(audio, key=BING_KEY)
                print("Microsoft Bing Voice Recognition thinks you said " + text)
                return text
            except sr.UnknownValueError:
                print("Microsoft Bing Voice Recognition could not understand audio")
                return None
            except sr.RequestError as e:
                print("Could not request results from Microsoft Bing Voice Recognition service; {0}".format(e))
                return None
