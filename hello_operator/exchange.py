#!/usr/bin/python
import json
import fuzzyOperator as operator

def runFunc(module, function):

	module = __import__(module, globals(), [function], -1)

	func = getattr(module, function)

	return func()

def switchboard(question):
	try:
		modules = operator.pickModule("skills/config.json")
		go = operator.pickBestMatch(modules, question)
		print go
		return runFunc(go["module"], go["func"])
	except:
		return "There was an error in the config or a module"
