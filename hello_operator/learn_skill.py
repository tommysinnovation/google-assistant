#!/usr/bin/python

import json, sys

CONFIG_FILE = "skills/config.json"


def new_entry(function, module, keywords):
	template = {
		"function" : function,
		"module" : module,
		"keywords" : keywords
	}

	return template

def add_to_file(data):

	with open(CONFIG_FILE) as f:
		current = f.read()

	if current == "":
		current = []
	else:
		current = json.loads(current)

	current.append(data)

	with open(CONFIG_FILE, "w") as f:
		f.write(json.dumps(current, indent=3))

def add(function, module, keywords):
	
	data = new_entry(function, module, keywords)

	print "Your new config entry is:"
	print json.dumps(data, indent=3)

	response = ""
	while response != "Y" and response != "N":
		response = raw_input("Would you like to add this automatically (y/n)?").upper()

	if response == "Y":
		print "Writing to %s..." % CONFIG_FILE
		add_to_file(data)		
		print "Success!"
	elif response == "N":
		print "Sweeeet"


def find(function, module, data):

	for i, c in enumerate(data):
		if c.get("function") == function:
			if c.get("module") == module:
				return i

	return -1

def remove_from_file(i, data):
	data.pop(i)
	with open(CONFIG_FILE, "w") as f:
		f.write(json.dumps(data, indent=3))

def remove(function, module):
	with open(CONFIG_FILE) as f:
		current = f.read()
	
	if current != "":
		current = json.loads(current)

	i = find(function, module, current)

	if i != -1:

		print "Are you sure you want to remove:"
		print json.dumps(current[i], indent=3)

		response = ""
		while response != "Y" and response != "N":
			response = raw_input("(y/n)?").upper()

		if response == "Y":
			remove_from_file(i, current)
		elif response == "N":
			print "Sweeeet"
	else:
		print "Function '%s.%s' not found in %s" % (module, function, CONFIG_FILE)

def main():

	if sys.argv[1] == "add":
		
		keys = []

		for key in sys.argv[4:]:
			keys.append(key)
		

		add(sys.argv[2], sys.argv[3], keys)

	elif sys.argv[1] == "remove":

		remove(sys.argv[2], sys.argv[3]) 

	elif sys.argv[1] == "print":

		with open(CONFIG_FILE) as f:
			current = f.read()

		if current == "":
			current = []
		else:
			current = json.loads(current)

			print json.dumps(current, indent=3)
			

	else:

		print "Unknown option '%s'" % sys.argv[1]

main()