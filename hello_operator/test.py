import exchange
import json
import text2speech
import RPi.GPIO as GPIO
import time

exitflag=False

def initHW():
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(18,GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(23,GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.add_event_detect(18, GPIO.FALLING, bouncetime=500)
    GPIO.add_event_detect(23, GPIO.FALLING, bouncetime=500)

def main():
    initHW()
    x = 0
    while not exitflag:
        #print ("loop ", x)
        x += 1
        if GPIO.event_detected(18) or GPIO.event_detected(23):
            print("Trigger event detected")
            text2speech.speak("Beep!")
            text = text2speech.speech2text()
            text2speech.speak(exchange.switchboard(text))

        #print("event Loop ended")
        time.sleep(1.5)

if __name__ == '__main__':
    main()
