#!/usr/bin/python2

# Copyright (C) 2017 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from __future__ import print_function

import argparse
import os.path
import json

import google.oauth2.credentials

from google.assistant.library import Assistant
from google.assistant.library.event import EventType
from google.assistant.library.file_helpers import existing_file

import RPi.GPIO as GPIO
import time

exitflag=False

def trace_event(event):
    #Pretty prints events.
    #print("process_event()")
    if event.type == EventType.ON_CONVERSATION_TURN_STARTED:
        print()
    print(event)
    if (event.type == EventType.ON_CONVERSATION_TURN_FINISHED and
            event.args and not event.args['with_follow_on_turn']):
        print()

def obey_events(event_to_handle):
    print("obey_events()")
    print("Got event with args ", str(event_to_handle.args))
    if event_to_handle.args:
        if "exit" in str(event_to_handle.args):
            print("exit detected")
            global exitflag
            exitflag=True

def initHW():
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(18,GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(23,GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.add_event_detect(18, GPIO.FALLING, bouncetime=500)
    GPIO.add_event_detect(23, GPIO.FALLING, bouncetime=500)

def start_assistant():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--credentials', type=existing_file,
                        metavar='OAUTH2_CREDENTIALS_FILE',
                        default=os.path.join(
                            os.path.expanduser('~/.config'),
                            'google-oauthlib-tool',
                            'credentials.json'
                        ),
                        help='Path to store and read OAuth2 credentials')
    args = parser.parse_args()
    with open(args.credentials, 'r') as f:
        credentials = google.oauth2.credentials.Credentials(token=None, **json.load(f))

    return Assistant(credentials)           

def main():
    initHW()
    with  start_assistant() as assistant:
        x = 0
        eventList = assistant.start()
        global exitflag
        while not exitflag:
            print ("loop ", x) 
            x += 1
            if GPIO.event_detected(18) or GPIO.event_detected(23):
                print("Trigger event detected")
                print("Starting Conversation")
                assistant.start_conversation()
            print("event list size =", str(eventList.qsize()))
            if not eventList.empty():
                for event in eventList:
                    print("event found being handled")
                    trace_event(event)
                    obey_events(event)
                    if eventList.empty():
                        print ("eventList now empty")
                        break
            print("event Loop ended")
            time.sleep(1.5)

if __name__ == '__main__':
    main()
